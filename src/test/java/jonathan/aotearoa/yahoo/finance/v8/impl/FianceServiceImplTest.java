package jonathan.aotearoa.yahoo.finance.v8.impl;

import jonathan.aotearoa.yahoo.finance.v8.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Disabled("Disabled integration test")
class FianceServiceImplTest {

    private static final ZoneId NZT = ZoneId.of("Pacific/Auckland");

    private FinanceService financeService;

    @BeforeEach
    public void setUp() {
        financeService = new FinanceServiceImpl();
    }

    @Test
    void getQuotes() throws Exception {
        final CurrencyPair currencyPair = CurrencyPair.of("GBP", "NZD");
        final Response response = financeService.getQuotes(currencyPair, Range.ONE_MONTH, Interval.ONE_HOUR);
        assertNotNull(response);
        assertNotNull(response.quotes());
        assertFalse(response.quotes().isEmpty());

        System.out.printf("Total quotes: %d%n", response.quotes().size());
        response.getQuotesByDate(NZT)
                .forEach((key, value) -> System.out.printf("Date: %s, Quotes: %d%n", key, value.size()));

        final LocalDate today = ZonedDateTime.now(NZT).toLocalDate();
        final Optional<List<Quote>> todayQuotes = response.getQuotes(today, NZT);
        assertTrue(todayQuotes.isPresent());
        assertFalse(todayQuotes.get().isEmpty());
        System.out.printf("Today's quotes: %d%n", todayQuotes.get().size());

        final Optional<List<BigDecimal>> range = response.getRange(today, NZT);
        assertTrue(range.isPresent());
        System.out.println(range.get());
    }
}
