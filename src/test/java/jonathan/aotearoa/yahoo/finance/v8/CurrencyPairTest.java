package jonathan.aotearoa.yahoo.finance.v8;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Currency;
import java.util.stream.Stream;

import static java.util.Objects.nonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CurrencyPairTest {

    private static final Currency GBP = Currency.getInstance("GBP");
    private static final Currency NZD = Currency.getInstance("NZD");

    @Test
    void constructor_validArgs() {
        final CurrencyPair currencyPair = new CurrencyPair(GBP, NZD);
        assertSame(GBP, currencyPair.from());
        assertSame(NZD, currencyPair.to());
    }

    @ParameterizedTest
    @MethodSource("constructorInvalidArgs")
    void constructor_invalidArgs(final Currency from,
                                 final Currency to,
                                 final String exceptionMsg) {
        final Exception e = assertThrows(IllegalArgumentException.class, () -> new CurrencyPair(from, to));
        if (nonNull(exceptionMsg)) assertThat(e.getMessage(), containsString(exceptionMsg));
    }

    private static Stream<Arguments> constructorInvalidArgs() {
        return Stream.of(
                Arguments.of(null, NZD, "@NotNull parameter 'from'"),
                Arguments.of(GBP, null, "@NotNull parameter 'to'"),
                Arguments.of(GBP, GBP, "from 'GBP' and to 'GBP' cannot be the same currency")
        );
    }

    @Test
    void of_validCodes() {
        final CurrencyPair currencyPair = CurrencyPair.of("GBP", "NZD");
        assertSame(GBP, currencyPair.from());
        assertSame(NZD, currencyPair.to());
    }

    @Test
    void of_invalidCode() {
        assertThrows(IllegalArgumentException.class, () -> CurrencyPair.of("FOO", "NZD"));
    }
}
