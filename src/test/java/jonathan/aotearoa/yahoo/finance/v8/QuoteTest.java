package jonathan.aotearoa.yahoo.finance.v8;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertThrows;

class QuoteTest {

    @ParameterizedTest
    @MethodSource("constructorInvalidArgs")
    void constructor_invalidArgs(final ZonedDateTime startDateTime,
                                 final Interval interval,
                                 final BigDecimal low,
                                 final BigDecimal high,
                                 final String exceptionMsg) {
        final Exception e = assertThrows(IllegalArgumentException.class, () -> new Quote(startDateTime, interval, low, high));
        if (nonNull(exceptionMsg)) assertThat(e.getMessage(), containsString(exceptionMsg));
    }

    private static Stream<Arguments> constructorInvalidArgs() {
        final ZonedDateTime now = ZonedDateTime.now();
        final ZonedDateTime future = now.plusYears(1);
        return Stream.of(
                Arguments.of(null, Interval.ONE_HOUR, null, null, "@NotNull parameter 'startDateTime'"),
                Arguments.of(now, null, null, null, "@NotNull parameter 'interval'"),
                Arguments.of(future, Interval.ONE_HOUR, null, null, format("startDateTime '%s' cannot be in the future", future)),
                Arguments.of(now, Interval.ONE_HOUR, BigDecimal.ONE, BigDecimal.ZERO, "high '0' cannot be less than low '1'")
        );
    }

    @ParameterizedTest
    @MethodSource("isBetweenInvalidArgs")
    void isBetween_invalidArgs(final ZonedDateTime from,
                               final ZonedDateTime to,
                               final String exceptionMsg) {
        final ZonedDateTime now = ZonedDateTime.now();
        final Quote quote = new Quote(now, Interval.ONE_HOUR, null, null);

        final Exception e = assertThrows(IllegalArgumentException.class, () -> quote.isBetween(from, to));
        if (nonNull(exceptionMsg)) assertThat(e.getMessage(), containsString(exceptionMsg));
    }

    private static Stream<Arguments> isBetweenInvalidArgs() {
        final ZonedDateTime now = ZonedDateTime.now();
        final ZonedDateTime nowPlusOneSec = now.plusSeconds(1);
        return Stream.of(
                Arguments.of(null, now, "@NotNull parameter 'from'"),
                Arguments.of(now, null, "@NotNull parameter 'to'"),
                Arguments.of(now, now, format("from '%s' must be before to '%s'", now, now)),
                Arguments.of(nowPlusOneSec, now, format("from '%s' must be before to '%s'", nowPlusOneSec, now))
        );
    }
}
