package jonathan.aotearoa.yahoo.finance.v8;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static java.util.Map.ofEntries;
import static jonathan.aotearoa.yahoo.finance.v8.Range.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RangeTest {

    private static final Map<String, Range> BY_SHORT_NAME = ofEntries(
            entry("1d", ONE_DAY),
            entry("5d", FIVE_DAYS),
            entry("1mo", ONE_MONTH),
            entry("3mo", THREE_MONTHS),
            entry("6mo", SIX_MONTHS),
            entry("ytd", YEAR_TO_DATE),
            entry("1y", ONE_YEAR),
            entry("2y", TWO_YEARS),
            entry("5y", FIVE_YEARS),
            entry("10y", TEN_YEARS),
            entry("max", MAX)
    );

    @Test
    void testsCoverAllValues() {
        assertEquals(BY_SHORT_NAME.size(), Range.values().length);
    }

    @Test
    void ofShortName_unknown() {
        final Exception e = assertThrows(IllegalArgumentException.class, () -> Range.ofShortName("blah"));
        assertEquals("Unknown shortName: 'blah'", e.getMessage());
    }

    @ParameterizedTest
    @MethodSource("ofShortNameArgs")
    void ofShortName(final String shortName, final Range expected) {
        assertEquals(expected, Range.ofShortName(shortName));
    }

    @ParameterizedTest
    @MethodSource("getShortNameArgs")
    void getShortName(final Range range, final String expected) {
        assertEquals(expected, range.shortName);
    }

    private static Stream<Arguments> ofShortNameArgs() {
        return BY_SHORT_NAME.entrySet().stream()
                .map(entry -> Arguments.of(entry.getKey(), entry.getValue()));
    }

    private static Stream<Arguments> getShortNameArgs() {
        return BY_SHORT_NAME.entrySet().stream()
                .map(entry -> Arguments.of(entry.getValue(), entry.getKey()));
    }
}
