package jonathan.aotearoa.yahoo.finance.v8;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static java.util.Map.ofEntries;
import static jonathan.aotearoa.yahoo.finance.v8.Interval.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class IntervalTest {

    private static final Map<String, Interval> BY_SHORT_NAME = ofEntries(
            entry("1m", ONE_MINUTE),
            entry("5m", FIVE_MINUTES),
            entry("15m", FIFTEEN_MINUTES),
            entry("1h", ONE_HOUR),
            entry("1d", ONE_DAY),
            entry("1wk", ONE_WEEK),
            entry("1mo", ONE_MONTH)
    );

    @Test
    void testsCoverAllValues() {
        assertEquals(BY_SHORT_NAME.size(), Interval.values().length);
    }

    @Test
    void ofShortName_unknown() {
        final Exception e = assertThrows(IllegalArgumentException.class, () -> Interval.ofShortName("blah"));
        assertEquals("Unknown shortName: 'blah'", e.getMessage());
    }

    @ParameterizedTest
    @MethodSource("ofShortNameArgs")
    void ofShortName(final String shortName, final Interval expected) {
        assertEquals(expected, Interval.ofShortName(shortName));
    }

    @ParameterizedTest
    @MethodSource("getShortNameArgs")
    void getShortName(final Interval interval, final String expected) {
        assertEquals(expected, interval.shortName);
    }

    private static Stream<Arguments> ofShortNameArgs() {
        return BY_SHORT_NAME.entrySet().stream()
                .map(entry -> Arguments.of(entry.getKey(), entry.getValue()));
    }

    private static Stream<Arguments> getShortNameArgs() {
        return BY_SHORT_NAME.entrySet().stream()
                .map(entry -> Arguments.of(entry.getValue(), entry.getKey()));
    }
}
