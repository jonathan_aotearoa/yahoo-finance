package jonathan.aotearoa.yahoo.finance.v8.impl;

import jonathan.aotearoa.yahoo.finance.v8.FinanceApiException;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import okhttp3.mockwebserver.SocketPolicy;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DataFetcherTest {

    private static final String RESPONSE_BODY = "response-body";
    public static final String URL_PATH = "/dummy-endpoint";

    private MockWebServer server;
    private DataFetcher dataFetcher;

    @BeforeEach
    void setUp() {
        this.server = new MockWebServer();
        this.dataFetcher = new DataFetcher();
    }

    @AfterEach
    void tearDown() throws Exception {
        this.server.shutdown();
    }

    @Test
    void fetchJson_success() throws Exception {
        // Given
        this.server.enqueue(new MockResponse()
                .setBody(RESPONSE_BODY)
                .setResponseCode(200));
        server.start();
        // When
        final String responseBody = dataFetcher.fetchJson(getUrl());
        // Then
        assertEquals(RESPONSE_BODY, responseBody);
    }

    @Test
    void fetchJson_headers() throws Exception {
        // Given
        this.server.enqueue(new MockResponse()
                .setBody(RESPONSE_BODY)
                .setResponseCode(200));
        server.start();
        // When
        dataFetcher.fetchJson(getUrl());
        // Then
        final RecordedRequest request = this.server.takeRequest();
        DataFetcher.HEADERS.names().forEach(name -> {
            List<String> values = DataFetcher.HEADERS.values(name);
            assertEquals(values, request.getHeaders().values(name));
        });
    }

    @ParameterizedTest
    @ValueSource(ints = {300, 400, 500})
    void fetchJson_unsuccessful(final int statusCode) throws Exception {
        // Given
        this.server.enqueue(new MockResponse()
                .setBody(RESPONSE_BODY)
                .setResponseCode(statusCode));
        server.start();
        final HttpUrl url = getUrl();
        // Then
        final Exception e = assertThrows(FinanceApiException.class, () -> dataFetcher.fetchJson(url));
        final String expectedMsg = format("Error fetching data from '%s'. Response status code: %d", url, statusCode);
        assertEquals(expectedMsg, e.getMessage());
    }

    @Test
    void fetchJson_ioError() throws Exception {
        // Given
        this.server.enqueue(new MockResponse()
                .setBody(RESPONSE_BODY)
                .setResponseCode(200)
                .setSocketPolicy(SocketPolicy.DISCONNECT_DURING_RESPONSE_BODY));
        server.start();
        final HttpUrl url = getUrl();
        // Then
        final Exception e = assertThrows(FinanceApiException.class, () -> dataFetcher.fetchJson(getUrl()));
        final String expectedMsg = format("Error fetching data from '%s'", url);
        assertEquals(expectedMsg, e.getMessage());
        assertThat(e.getCause(), instanceOf(IOException.class));
    }

    private HttpUrl getUrl() {
        final URL url = this.server.url(URL_PATH).url();
        return requireNonNull(HttpUrl.get(url));
    }
}
