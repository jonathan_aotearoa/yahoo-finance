package jonathan.aotearoa.yahoo.finance.v8.factory;

import jonathan.aotearoa.yahoo.finance.v8.FinanceService;
import jonathan.aotearoa.yahoo.finance.v8.impl.FinanceServiceImpl;

public class ServiceFactory {

    private static final ServiceFactory INSTANCE = new ServiceFactory();

    private ServiceFactory() {
    }

    public FinanceService newFinanceService() {
        return new FinanceServiceImpl();
    }

    public static ServiceFactory getInstance() {
        return INSTANCE;
    }
}
