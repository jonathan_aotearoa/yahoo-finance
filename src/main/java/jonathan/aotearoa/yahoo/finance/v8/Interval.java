package jonathan.aotearoa.yahoo.finance.v8;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

import static java.lang.String.format;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toMap;

public enum Interval {

    ONE_MINUTE("1m"),
    FIVE_MINUTES("5m"),
    FIFTEEN_MINUTES("15m"),
    ONE_HOUR("1h"),
    ONE_DAY("1d"),
    ONE_WEEK("1wk"),
    ONE_MONTH("1mo");

    private static final Map<String, Interval> BY_SHORT_NAME = Arrays.stream(Interval.values())
            .collect(collectingAndThen(
                    toMap(Interval::getShortName, Function.identity()),
                    Collections::unmodifiableMap));

    @NotNull
    public final String shortName;

    Interval(@NotNull final String shortName) {
        this.shortName = shortName;
    }

    @NotNull
    static Interval ofShortName(@NotNull final String shortName) {
        final Interval value = BY_SHORT_NAME.get(shortName);
        if (value == null) {
            throw new IllegalArgumentException(format("Unknown shortName: '%s'", shortName));
        }
        return value;
    }

    @NotNull
    private String getShortName() {
        return this.shortName;
    }
}
