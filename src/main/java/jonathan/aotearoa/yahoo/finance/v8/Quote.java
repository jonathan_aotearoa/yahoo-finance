package jonathan.aotearoa.yahoo.finance.v8;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import static java.time.ZonedDateTime.now;
import static java.util.Objects.isNull;
import static org.apache.commons.lang3.Validate.isTrue;

public record Quote(@NotNull ZonedDateTime startDateTime,
                    @NotNull Interval interval,
                    BigDecimal low,
                    BigDecimal high) implements Comparable<Quote> {

    public Quote {
        isTrue(startDateTime.isBefore(now()), "startDateTime '%s' cannot be in the future", startDateTime);
        isTrue(isNull(low) || isNull(high) || high.compareTo(low) >= 0, "high '%s' cannot be less than low '%s'", high, low);
    }

    public boolean isBetween(@NotNull final ZonedDateTime from, @NotNull final ZonedDateTime to) {
        isTrue(from.isBefore(to), "from '%s' must be before to '%s'", from, to);
        return this.startDateTime().compareTo(from) >= 0 && this.startDateTime().compareTo(to) < 0;
    }

    @Override
    public int compareTo(@NotNull Quote other) {
        return this.startDateTime.compareTo(other.startDateTime);
    }
}
