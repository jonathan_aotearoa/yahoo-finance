package jonathan.aotearoa.yahoo.finance.v8;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

import static java.lang.String.format;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toMap;

public enum Range {

    ONE_DAY("1d"),
    FIVE_DAYS("5d"),
    ONE_MONTH("1mo"),
    THREE_MONTHS("3mo"),
    SIX_MONTHS("6mo"),
    ONE_YEAR("1y"),
    TWO_YEARS("2y"),
    FIVE_YEARS("5y"),
    TEN_YEARS("10y"),
    YEAR_TO_DATE("ytd"),
    MAX("max");

    private static final Map<String, Range> BY_SHORT_NAME = Arrays.stream(Range.values())
            .collect(collectingAndThen(
                    toMap(Range::getShortName, Function.identity()),
                    Collections::unmodifiableMap));

    @NotNull
    public final String shortName;

    Range(@NotNull final String shortName) {
        this.shortName = shortName;
    }

    @NotNull
    static Range ofShortName(@NotNull final String shortName) {
        final Range value = BY_SHORT_NAME.get(shortName);
        if (value == null) {
            throw new IllegalArgumentException(format("Unknown shortName: '%s'", shortName));
        }
        return value;
    }

    @NotNull
    private String getShortName() {
        return this.shortName;
    }
}
