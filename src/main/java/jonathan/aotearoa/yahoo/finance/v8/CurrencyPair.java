package jonathan.aotearoa.yahoo.finance.v8;

import org.jetbrains.annotations.NotNull;

import java.util.Currency;

import static org.apache.commons.lang3.Validate.isTrue;

public record CurrencyPair(@NotNull Currency from, @NotNull Currency to) {

    public CurrencyPair {
        isTrue(!from.equals(to), "from '%s' and to '%s' cannot be the same currency", from, to);
    }

    @NotNull
    public static CurrencyPair of(@NotNull String fromCode, @NotNull String toCode) {
        return new CurrencyPair(Currency.getInstance(fromCode), Currency.getInstance(toCode));
    }
}
