package jonathan.aotearoa.yahoo.finance.v8.impl;

import jonathan.aotearoa.yahoo.finance.v8.CurrencyPair;
import jonathan.aotearoa.yahoo.finance.v8.Interval;
import jonathan.aotearoa.yahoo.finance.v8.Range;
import okhttp3.HttpUrl;
import org.apache.commons.lang3.ObjectUtils;
import org.jetbrains.annotations.NotNull;

import static java.lang.String.format;

class ChartUrlBuilder {

    private static final Range DEFAULT_RANGE = Range.ONE_MONTH;
    private static final Interval DEFAULT_INTERVAL = Interval.ONE_HOUR;

    private final CurrencyPair currencyPair;
    private Range range;
    private Interval interval;

    ChartUrlBuilder(@NotNull final CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
        this.range = DEFAULT_RANGE;
        this.interval = DEFAULT_INTERVAL;
    }

    ChartUrlBuilder withRange(final Range range) {
        this.range = ObjectUtils.defaultIfNull(range, DEFAULT_RANGE);
        return this;
    }

    ChartUrlBuilder withInterval(final Interval interval) {
        this.interval = ObjectUtils.defaultIfNull(interval, DEFAULT_INTERVAL);
        return this;
    }

    @NotNull
    HttpUrl build() {
        final String symbol = format("%s%s=X", currencyPair.from().getCurrencyCode(), currencyPair.to().getCurrencyCode());

        return new HttpUrl.Builder()
                .scheme("https")
                .host("query1.finance.yahoo.com")
                .addPathSegments("v8/finance/chart")
                .addPathSegment(symbol)
                .addQueryParameter("symbol", symbol)
                .addQueryParameter("range", this.range.shortName)
                .addQueryParameter("interval", this.interval.shortName)
                .addQueryParameter("includePrePost", "false")
                .addQueryParameter("lang", "en-US")
                .addQueryParameter("region", "US")
                .addQueryParameter("corsDomain", "finance.yahoo.com")
                .build();
    }
}
