package jonathan.aotearoa.yahoo.finance.v8.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import jonathan.aotearoa.yahoo.finance.v8.FinanceApiException;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.util.Set;

class JsonValidator {

    private final JsonSchema jsonSchema;
    private final ObjectMapper objectMapper;

    private JsonValidator(@NotNull final InputStream schemaStream) {
        final JsonSchemaFactory schemaFactory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V7);
        this.jsonSchema = schemaFactory.getSchema(schemaStream);
        this.objectMapper = new ObjectMapper();
    }

    @NotNull
    static JsonValidator newChartResponseValidator() {
        return new JsonValidator(ResourceUtils.getResource("v8-chart-schema.json"));
    }

    void validate(@NotNull final String json) throws FinanceApiException {
        final JsonNode jsonNode;
        try {
            jsonNode = this.objectMapper.readTree(json);
        } catch (JsonProcessingException e) {
            throw new FinanceApiException("Error validating Yahoo Finance API response", e);
        }
        Set<ValidationMessage> errors = this.jsonSchema.validate(jsonNode);
        if (errors.isEmpty()) return;
        throw new InvalidResponseException(errors.stream().map(ValidationMessage::toString).toList());
    }
}
