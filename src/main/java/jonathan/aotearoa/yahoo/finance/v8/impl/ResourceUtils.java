package jonathan.aotearoa.yahoo.finance.v8.impl;

import org.jetbrains.annotations.NotNull;

import java.io.InputStream;

import static java.lang.String.format;

final class ResourceUtils {

    private ResourceUtils() {
    }

    @NotNull
    static InputStream getResource(@NotNull final String resourceName) {
        final InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceName);
        if (inputStream == null) {
            throw new IllegalArgumentException(format("Resource '%s' does not exist", resourceName));
        }
        return inputStream;
    }
}
