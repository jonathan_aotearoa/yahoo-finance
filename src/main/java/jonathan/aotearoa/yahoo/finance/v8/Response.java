package jonathan.aotearoa.yahoo.finance.v8;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import static java.util.Collections.binarySearch;
import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.lang3.Validate.noNullElements;

public record Response(@NotNull List<Quote> quotes) {

    public Response {
        noNullElements(quotes, "quotes cannot be null or contain null");
    }

    @NotNull
    public List<Quote> getQuotesBetween(@NotNull final ZonedDateTime from, @NotNull final ZonedDateTime to) {
        isTrue(from.isBefore(to), "from %s cannot be after to %s", from, to);

        return quotes.stream()
                .filter(quote -> quote.isBetween(from, to))
                .toList();
    }

    @NotNull
    public TreeMap<LocalDate, List<Quote>> getQuotesByDate(@NotNull final ZoneId zoneId) {
        return quotes.stream().collect(groupingBy(
                quote -> quote.startDateTime().withZoneSameInstant(zoneId).toLocalDate(),
                TreeMap::new,
                toList()));
    }

    @NotNull
    public Optional<List<Quote>> getQuotes(@NotNull final LocalDate date, @NotNull final ZoneId zoneId) {
        final TreeMap<LocalDate, List<Quote>> quotesByDate = getQuotesByDate(zoneId);
        final List<LocalDate> dates = quotesByDate.keySet().stream().toList();
        final int searchResult = binarySearch(dates, date);
        if (searchResult == -1) return Optional.empty();
        final int dateIndex = searchResult >= 0 ? searchResult : -searchResult - 2;
        final LocalDate closestDate = dates.get(dateIndex);
        return Optional.of(quotesByDate.get(closestDate));
    }

    @NotNull
    public Optional<List<BigDecimal>> getRange(@NotNull final LocalDate date, @NotNull final ZoneId zoneId) {
        final Optional<List<Quote>> optional = getQuotes(date, zoneId);
        if (optional.isEmpty()) {
            return Optional.empty();
        }
        final List<Quote> quotesForDate = optional.get();
        @Nullable final BigDecimal low = quotesForDate.stream()
                .map(Quote::low)
                .filter(Objects::nonNull)
                .min(BigDecimal::compareTo)
                .orElse(null);
        @Nullable final BigDecimal high = quotesForDate.stream()
                .map(Quote::high)
                .filter(Objects::nonNull)
                .max(BigDecimal::compareTo)
                .orElse(null);
        final List<BigDecimal> range = new ArrayList<>();
        range.add(low);
        range.add(high);
        return Optional.of(unmodifiableList(range));
    }
}
