package jonathan.aotearoa.yahoo.finance.v8.impl;

import jonathan.aotearoa.yahoo.finance.v8.FinanceApiException;
import okhttp3.*;
import okhttp3.brotli.BrotliInterceptor;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;
import static okhttp3.Protocol.HTTP_1_1;


class DataFetcher {

    static final Headers HEADERS = Headers.of(
            "Host", "query1.finance.yahoo.com",
            "Accept", "*/*",
            "Accept-Language", "en-US,en;q=0.5",
            "Origin", "https://finance.yahoo.com",
            "DNT", "1",
            "Connection", "keep-alive",
            "Sec-Fetch-Dest", "empty",
            "Sec-Fetch-Mode", "cors",
            "Sec-Fetch-Site", "same-site",
            "TE", "trailers"
    );

    private final OkHttpClient client;

    DataFetcher() {
        this.client = new OkHttpClient.Builder()
                .protocols(singletonList(HTTP_1_1))
                .addInterceptor(BrotliInterceptor.INSTANCE)
                .build();
    }

    @NotNull
    String fetchJson(@NotNull final HttpUrl url) throws FinanceApiException {
        final Request request = new Request.Builder()
                .url(url)
                .headers(HEADERS)
                .build();
        try {
            try (final Response response = client.newCall(request).execute()) {
                if (response.isSuccessful()) {
                    return requireNonNull(response.body()).string();
                }
                throw new FinanceApiException("Error fetching data from '%s'. Response status code: %d", url, response.code());
            }
        } catch (IOException e) {
            throw new FinanceApiException(e, "Error fetching data from '%s'", url);
        }
    }
}
