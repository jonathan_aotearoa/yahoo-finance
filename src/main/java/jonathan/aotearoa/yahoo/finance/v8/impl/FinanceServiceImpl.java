package jonathan.aotearoa.yahoo.finance.v8.impl;

import jonathan.aotearoa.yahoo.finance.v8.*;
import okhttp3.HttpUrl;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FinanceServiceImpl implements FinanceService {

    private static final Logger LOG = LoggerFactory.getLogger(FinanceServiceImpl.class);

    private final DataFetcher dataFetcher;
    private final JsonValidator chartResponseValidator;
    private final ResponseBuilder responseBuilder;

    public FinanceServiceImpl() {
        this(new DataFetcher(), JsonValidator.newChartResponseValidator(), new ResponseBuilder());
    }

    FinanceServiceImpl(@NotNull final DataFetcher dataFetcher,
                       @NotNull final JsonValidator chartResponseValidator,
                       @NotNull final ResponseBuilder responseBuilder) {
        this.dataFetcher = dataFetcher;
        this.chartResponseValidator = chartResponseValidator;
        this.responseBuilder = responseBuilder;
    }

    @Override
    @NotNull
    public Response getQuotes(@NotNull CurrencyPair currencyPair,
                              @NotNull Range range,
                              @NotNull Interval interval) throws FinanceApiException {
        LOG.debug("Getting quotes. {}, {}, {}", currencyPair, range, interval);

        HttpUrl url = new ChartUrlBuilder(currencyPair)
                .withRange(range)
                .withInterval(interval)
                .build();
        LOG.debug("URI: {}", url);

        final String json = dataFetcher.fetchJson(url);
        LOG.debug("Successfully fetched JSON. {}", json);

        this.chartResponseValidator.validate(json);
        return this.responseBuilder.createResponse(json, interval);
    }
}
