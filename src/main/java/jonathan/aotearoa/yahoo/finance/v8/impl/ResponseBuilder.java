package jonathan.aotearoa.yahoo.finance.v8.impl;

import com.jayway.jsonpath.*;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;
import jonathan.aotearoa.yahoo.finance.v8.Interval;
import jonathan.aotearoa.yahoo.finance.v8.Quote;
import jonathan.aotearoa.yahoo.finance.v8.Response;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

class ResponseBuilder {

    private static final TypeRef<List<Long>> LONG_LIST_TYPE = new TypeRef<>() {
    };
    private static final TypeRef<List<BigDecimal>> BIG_DECIMAL_LIST_TYPE = new TypeRef<>() {
    };

    ResponseBuilder() {
        Configuration.setDefaults(new Configuration.Defaults() {

            private final JsonProvider jsonProvider = new JacksonJsonProvider();
            private final MappingProvider mappingProvider = new JacksonMappingProvider();

            @Override
            public JsonProvider jsonProvider() {
                return jsonProvider;
            }

            @Override
            public MappingProvider mappingProvider() {
                return mappingProvider;
            }

            @Override
            public Set<Option> options() {
                return EnumSet.noneOf(Option.class);
            }
        });
    }

    @NotNull
    public Response createResponse(@NotNull final String json, @NotNull final Interval interval) {
        final ReadContext readCtx = JsonPath.parse(json);
        final String timeZone = readCtx.read("$.chart.result[0].meta.exchangeTimezoneName");
        final List<Long> timestamps = readCtx.read("$.chart.result[0].timestamp", LONG_LIST_TYPE);
        final List<BigDecimal> highs = readCtx.read("$.chart.result[0].indicators.quote[0].high", BIG_DECIMAL_LIST_TYPE);
        final List<BigDecimal> lows = readCtx.read("$.chart.result[0].indicators.quote[0].low", BIG_DECIMAL_LIST_TYPE);

        final ZoneId zoneId = ZoneId.of(timeZone);
        final List<ZonedDateTime> dateTimes = asDateTimes(timestamps, zoneId);
        final List<Quote> quotes = IntStream.range(0, dateTimes.size()).mapToObj(i ->
                new Quote(dateTimes.get(i), interval, lows.get(i), highs.get(i))
        ).toList();
        return new Response(quotes);
    }

    private static List<ZonedDateTime> asDateTimes(@NotNull final List<Long> timestamps, @NotNull final ZoneId zoneId) {
        return timestamps.stream()
                .map(Instant::ofEpochSecond)
                .map(instant -> ZonedDateTime.ofInstant(instant, zoneId))
                .toList();
    }
}
