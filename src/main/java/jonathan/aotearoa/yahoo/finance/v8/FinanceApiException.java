package jonathan.aotearoa.yahoo.finance.v8;

import static java.lang.String.format;

public class FinanceApiException extends Exception {

    public FinanceApiException(String message, Object... messageArgs) {
        super(format(message, messageArgs));
    }

    public FinanceApiException(Throwable cause, String message, Object... messageArgs) {
        super(format(message, messageArgs), cause);
    }

    public FinanceApiException(String message) {
        super(message);
    }

    public FinanceApiException(String message, Throwable cause) {
        super(message, cause);
    }
}
