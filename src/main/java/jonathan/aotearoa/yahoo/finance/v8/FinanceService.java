package jonathan.aotearoa.yahoo.finance.v8;

import org.jetbrains.annotations.NotNull;

public interface FinanceService {

    @NotNull
    Response getQuotes(@NotNull CurrencyPair currencyPair,
                       @NotNull Range range,
                       @NotNull Interval interval) throws FinanceApiException;
}
