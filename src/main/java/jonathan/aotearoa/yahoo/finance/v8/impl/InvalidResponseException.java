package jonathan.aotearoa.yahoo.finance.v8.impl;

import jonathan.aotearoa.yahoo.finance.v8.FinanceApiException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.Validate.isTrue;
import static org.apache.commons.lang3.Validate.noNullElements;

public class InvalidResponseException extends FinanceApiException {

    private final List<String> errors;

    public InvalidResponseException(@NotNull final String error) {
        this(singletonList(error));
    }

    public InvalidResponseException(@NotNull final List<String> errors) {
        super(format("Invalid Yahoo Finance API response. Found %d validation errors", getSize(errors)));
        noNullElements(errors, "errors cannot be null or contain null");
        isTrue(!errors.isEmpty(), "errors cannot be empty");
        this.errors = errors;
    }

    public List<String> getErrors() {
        return this.errors;
    }

    private static int getSize(final List<String> errors) {
        return errors == null ? 0 : errors.size();
    }
}
