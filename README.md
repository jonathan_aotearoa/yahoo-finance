# Yahoo Finance

A Java API for retrieving Yahoo Finance API data.

## Example Usage

```java

import jonathan.aotearoa.yahoo.finance.v8.FinanceService;
import jonathan.aotearoa.yahoo.finance.v8.factory.ServiceFactory;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

class ExchangeRateTest {

    private static final ZoneId NZT = ZoneId.of("Pacific/Auckland");

    public static main(String[] args) {
        final FinanceService financeService = ServiceFactory.getInstance().newFinanceService();
        final LocalDate today = ZonedDateTime.now(NZT).toLocalDate();
        final Optional<List<BigDecimal>> rangeOptional = financeService.getRange(today, NZT);
        if (rangeOptional.isPresent()) {
            final BigDecimal low = rangeOptional.get().get(0);
            final BigDecimal high = rangeOptional.get().get(1);
            System.out.printf("Today's high: %s, low: %s%n", );
        }
    }
}


```